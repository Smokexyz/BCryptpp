#include <iostream>
#include "BCrypt.hpp"

int main()
{
	std::string password;

	password = BCrypt::generateHash("ASDFGH", 12);
	std::cout << password << std::endl;
	std::cout << (BCrypt::validatePassword("ABCDEF", password) ? "true" : "false") << std::endl;
	std::cout << (BCrypt::validatePassword("ASDFGH", password) ? "true" : "false") << std::endl;
	return 0;
}